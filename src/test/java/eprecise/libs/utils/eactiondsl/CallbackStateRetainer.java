
package eprecise.libs.utils.eactiondsl;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;


public class CallbackStateRetainer {

    private final Collection<CallLog> log = Collections.synchronizedList(new ArrayList<>());

    public void call(String callbackName) {
        this.log.add(new CallLog(callbackName));
    }

    public <T> T call(String callbackName, T callbackResult) {
        this.log.add(new CallLog(callbackName));
        return callbackResult;
    }

    public Collection<CallLog> getCalls(String callbackName) {
        return this.log.stream().filter(l -> l.name.equals(callbackName)).sorted().collect(toList());
    }

    public class CallLog implements Comparable<CallLog> {

        private final String name;

        private final long time = System.nanoTime();

        public CallLog(String name) {
            this.name = name;
        }

        public long getTime() {
            return this.time;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof CallLog) {
                final CallLog other = (CallLog) obj;
                return this.name.equals(other.name) && this.time == other.time;
            }
            return false;
        }

        @Override
        public int compareTo(CallLog o) {
            return (int) (this.time - o.time);
        }

    }

}
