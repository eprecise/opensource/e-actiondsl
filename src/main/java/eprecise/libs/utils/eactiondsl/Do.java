
package eprecise.libs.utils.eactiondsl;

import java.util.function.Consumer;

import eprecise.libs.utils.eactiondsl.action.function.ActionFunction;
import eprecise.libs.utils.eactiondsl.action.function.ActionFunctionCallBuilder;
import eprecise.libs.utils.eactiondsl.action.procedure.ActionProcedure;
import eprecise.libs.utils.eactiondsl.action.procedure.ActionProcedureCallBuilder;
import eprecise.libs.utils.eactiondsl.async.DefaultAsyncRunnerFactory;


public class Do {

    private static final DefaultAsyncRunnerFactory ASYNC_RUNNER = new DefaultAsyncRunnerFactory();

    private static final Consumer<Exception> DEFAULT_ERROR_HANDLER = e -> {
        throw e instanceof RuntimeException ? (RuntimeException) e : new RuntimeException(e);
    };

    public static ActionProcedureCallBuilder it(ActionProcedure action) {
        return new ActionProcedureCallBuilder(action, ASYNC_RUNNER.getAsyncProcedureRunner(), DEFAULT_ERROR_HANDLER);
    }

    public static <T> ActionFunctionCallBuilder<T> it(ActionFunction<T> action) {
        return new ActionFunctionCallBuilder<T>(action, ASYNC_RUNNER.getAsyncFunctionRunner(), DEFAULT_ERROR_HANDLER);
    }

}
