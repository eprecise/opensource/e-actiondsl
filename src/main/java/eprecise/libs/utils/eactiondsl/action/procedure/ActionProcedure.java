
package eprecise.libs.utils.eactiondsl.action.procedure;

@FunctionalInterface
public interface ActionProcedure {

    void execute() throws Exception;
}
