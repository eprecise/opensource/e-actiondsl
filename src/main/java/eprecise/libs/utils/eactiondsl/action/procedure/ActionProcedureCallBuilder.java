
package eprecise.libs.utils.eactiondsl.action.procedure;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;


public class ActionProcedureCallBuilder {

    private final ActionProcedure procedure;

    private final Map<Predicate<Exception>, Consumer<Exception>> specificsErrorHandlers = new HashMap<>();

    private Consumer<Exception> errorHandler;

    private final Function<Runnable, Future<?>> asyncRunner;

    private final List<Runnable> beforeCallbacks = new ArrayList<>();

    private final List<Runnable> successCallbacks = new ArrayList<>();

    private final List<Runnable> afterCallbacks = new ArrayList<>();

    public ActionProcedureCallBuilder(ActionProcedure procedure, Function<Runnable, Future<?>> asyncRunner, Consumer<Exception> errorHandler) {
        this(procedure, asyncRunner, errorHandler, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyMap());
    }

    public ActionProcedureCallBuilder(ActionProcedure procedure, Function<Runnable, Future<?>> asyncRunner, Consumer<Exception> errorHandler, List<Runnable> beforeCallbacks,
            List<Runnable> successCallbacks, List<Runnable> afterCallbacks, Map<Predicate<Exception>, Consumer<Exception>> specificErrorHandler) {
        this.procedure = procedure;
        this.asyncRunner = asyncRunner;
        this.errorHandler = errorHandler;
        this.afterCallbacks.addAll(afterCallbacks);
        this.beforeCallbacks.addAll(beforeCallbacks);
        this.successCallbacks.addAll(successCallbacks);
        this.specificsErrorHandlers.putAll(specificErrorHandler);

    }

    public ActionProcedureCallBuilder before(Runnable run) {
        this.beforeCallbacks.add(run);
        return this;
    }

    public ActionProcedureCallBuilder onSuccess(Runnable run) {
        this.successCallbacks.add(run);
        return this;
    }

    public ActionProcedureCallBuilder after(Runnable run) {
        this.afterCallbacks.add(run);
        return this;
    }

    public ActionProcedureCallBuilder onError(Consumer<Exception> errorHandler) {
        this.errorHandler = errorHandler;
        return this;
    }

    public ActionProcedureCallBuilder onError(Predicate<Exception> consumePredicate, Consumer<Exception> consumer) {
        this.specificsErrorHandlers.put(consumePredicate, consumer);
        return this;
    }

    public <T extends Exception> ActionProcedureCallBuilder onError(Class<T> type, Consumer<T> consumer) {
        this.onError(e -> type.isInstance(e), e -> consumer.accept(type.cast(e)));
        return this;
    }

    public void sync() {
        try {
            this.beforeCallbacks.forEach(Runnable::run);
            this.procedure.execute();
            this.successCallbacks.forEach(Runnable::run);
        } catch (final Exception e) {
            final List<Consumer<Exception>> specficConsumers = this.specificsErrorHandlers.entrySet().stream().filter(kv -> kv.getKey().test(e)).map(kv -> kv.getValue()).collect(toList());
            if (specficConsumers.isEmpty()) {
                this.errorHandler.accept(e);
            } else {
                specficConsumers.forEach(c -> c.accept(e));
            }
        } finally {
            this.afterCallbacks.forEach(Runnable::run);
        }
    }

    public Future<?> async() {
        return this.asyncRunner.apply(this::sync);
    }

}
