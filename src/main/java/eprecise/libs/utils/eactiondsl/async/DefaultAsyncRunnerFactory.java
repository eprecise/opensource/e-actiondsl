
package eprecise.libs.utils.eactiondsl.async;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.function.Supplier;


public class DefaultAsyncRunnerFactory {

    private final ExecutorService executor = Executors.newCachedThreadPool();

    public Function<Runnable, Future<?>> getAsyncProcedureRunner() {
        return this.executor::submit;
    }

    public <T> Function<Supplier<T>, Future<T>> getAsyncFunctionRunner() {
        return s -> this.executor.submit(s::get);
    }
}
